package com.suntack.wizardcounter

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import com.suntack.wizardcounter.model.Game
import com.suntack.wizardcounter.persistence.Persistence

import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.util.*

class MainActivity : AppCompatActivity() {

    var persistence: Persistence? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        persistence = Persistence(filesDir)
        val files = persistence?.loadGames()
        files?.let {
            for(i in files){
                createTableRow("Spiel$i");
            }
        }

    }

    fun createTableRow(gameName:String){
        val tableLayout = findViewById<TableLayout>(R.id.table_layout)

        val game = Game("Gamename", Date())
        val tableRow = TableRow(this)
        tableRow.layoutParams = TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT)
        val button = Button(this)
        button.setText("Delete")
        button.setOnClickListener({ v -> System.out.println("clicke"); persistence?.storeGame(game);
            System.out.println(persistence?.loadGames())
        })
        val tv = TextView(this)
        tv.setText(gameName)
        tableRow.addView(tv)
        tableRow.addView(button)
        tableLayout.addView(tableRow)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return false
    }
}
