package com.suntack.wizardcounter.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class Game(@JsonProperty val name: String,@JsonProperty val date: Date){
    private var players: MutableList<Player> = mutableListOf()

    fun addPlayer(player:Player){
        players.add(player)
    }

    fun removePlayer(player:Player){
        players.remove(player)
    }

}