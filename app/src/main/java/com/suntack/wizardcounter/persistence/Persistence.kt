package com.suntack.wizardcounter.persistence

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import com.suntack.wizardcounter.model.Game
import java.io.File

class Persistence(val directory: File){
    val mapper = ObjectMapper().registerModule(KotlinModule())

    fun loadGame(name:String):Game{
        System.out.println(directory.absolutePath)
        val file = File(directory.absolutePath + "/" + name)
        return mapper.readValue<Game>(file)
    }

    fun storeGame(game: Game){
        val file = File(directory.absolutePath +"/" + game.name)
        mapper.writeValue(file,game)
    }

    fun loadGames():List<String>{
        val files = ArrayList<String>()
        for(f in directory.listFiles()){
            files.add(f.name)
        }
        return files
    }
}